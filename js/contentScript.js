var icon = chrome.extension.getURL("images/lyric.png");

function addButton()
{
	$("#player-options").append("<a id='lyricsButton' class='player-option' title='Letra'><img class='LPFGsongIcon' src='"+icon+"'/></a>");
	$("#volume-group").css("right", '130px');
	$("#now-playing > .inner").css("right", '178px');
}

$(document).ready(addButton());

var buttonLink = document.getElementById('lyricsButton');

buttonLink.addEventListener("click", function(){
	//Retrieving song info
	var song = $("#now-playing-metadata > a").text();
	var author = $("#now-playing-metadata > div > a").text();

	//TODO
	//debugger;

	//Get xy position of the button
	var xy = this.xy();
	//Generating popup window for lyric if not exists or removing in other case
	if($("#lyricContainer").length===0){
		$("body").append('<div id="lyricContainer" class="jjmenu" style="left:'+xy.x+'px;top:'+(xy.y-50)+'px">'+song+'-'+author+'</div>');
	}
	else{
		$("#lyricsContainer").remove();
	}

}, false);

HTMLElement.prototype.xy = function() {
	o = this;
	var l =o.offsetLeft; var t = o.offsetTop;
	while (o=o.offsetParent)
		l += o.offsetLeft;
	o = this;
	while (o=o.offsetParent)
		t += o.offsetTop;
	return {x:l,y:t};
}